// Add Your Scripts here
// HAMBURGER-AND-MENU-FUNCTION
$(document).ready(function(){
$('.nav-header').click(function(){
              $('.nav-header').toggleClass('active');
              // $('.collapse-nav').toggle('boxopened', 'easeInQuad');
                $('.collapse-nav').slideToggle("slow");
            });
$('#toggle-menu').click(function(){
			$(this).toggleClass('toggle-menu-visible');
			$(this).toggleClass('toggle-menu-hidden');
            });

    $('.menu-item-has-children').on('click', (event) => {

        if(event.target.nodeName=='svg'||event.target.nodeName=='path'){
            $(event.target).closest('a').siblings('.sub-menu')
                .slideToggle("");
        }else{
            $(event.target).siblings('.sub-menu')
                .slideToggle("");
        }
    });

    $(document).click(function(e) {
        $('.main_menu .menu-item-has-children')
            .not($('.menu-item-has-children').has($(e.target)))
            .children('.sub-menu')
            .slideUp("");
    });
});
// HAMBURGER-AND-MENU-FUNCTION-END