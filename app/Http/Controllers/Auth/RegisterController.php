<?php

namespace App\Http\Controllers\Auth;

use App\Country;
use App\DegreeLevel;
use App\Gender;
use App\JobExperience;
use App\JobTitle;
use App\JobTitleOther;
use App\JobType;
use App\Language;
use App\ProfileSummary;
use App\SalaryPeriod;
use App\State;
use App\User;
use App\Http\Requests;
use App\UserJobTitle;
use App\UserJobType;
use App\UserLanguage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Jrean\UserVerification\Traits\VerifiesUsers;
use Jrean\UserVerification\Facades\UserVerification;
use App\Http\Requests\Front\UserFrontRegisterFormRequest;
use App\Http\Requests\Front\UserFrontRegisterEditFormRequest;
use Illuminate\Auth\Events\Registered;
use App\Events\UserRegistered;
use DB;


class RegisterController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Register Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users as well as their
      | validation and creation. By default this controller uses a trait to
      | provide this functionality without requiring any additional code.
      |
     */

use RegistersUsers;
    use VerifiesUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => ['getVerification', 'getVerificationError']]);
    }
    public function showRegistrationForm()
    {
        $genders = Gender::where('is_active',1)->orderBy('sort_order', 'ASC')->get();
        $country = Country::where('country', 'Canada')->first();
        $states = State::where('is_active',1)->where('country_id', $country->id)->orderBy('state', 'ASC')->get();
        $jobTitles = JobTitle::where('is_active',1)->orderBy('job_title', 'ASC')->get();
        $jobTypes = JobType::where('is_active',1)->orderBy('sort_order', 'ASC')->get();
        $degreeLevels = DegreeLevel::where('is_active',1)->orderBy('sort_order', 'ASC')->get();
        $languages = Language::where('is_active',1)->orderBy('sort_order', 'ASC')->get();
        $salaryPeriods = SalaryPeriod::where('is_active',1)->orderBy('sort_order', 'ASC')->get();
        $jobExperiences = JobExperience::where('is_active',1)->orderBy('sort_order', 'ASC')->get();

        return view('auth.register')
            ->with('genders', $genders)
            ->with('country', $country)
            ->with('states', $states)
            ->with('jobTitles', $jobTitles)
            ->with('jobTypes', $jobTypes)
            ->with('languages', $languages)
            ->with('salaryPeriods', $salaryPeriods)
            ->with('jobExperiences', $jobExperiences)
            ->with('degreeLevels', $degreeLevels);
    }

    public function register(Request $request)
    {
        DB::table('users')->where('email', $request->email)->where('is_accept_terms', 0)->delete();
        $rules = [
            'first_name' => 'required|max:80',
            'last_name' => 'required|max:80',
            'phone' => 'max:20',
            'email' => 'required|unique:users,email|email|max:100',
            'password' => 'required|confirmed|min:8|max:50',
            'password_confirmation' => 'required|min:8|max:50',
            'gender' => 'required',
            'education' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'experience' => 'required',
            'postal_code' => 'required|max:100',
            'job_title' => 'required',
            'job_type' => 'required',
            'language' => 'required',
            'is_immediate_available' => 'required',
            'willing_to_relocate' => 'required',
            'profile_visibility' => 'required',
            'g-recaptcha-response' => 'required',
        ];

        $customMessages = [
            'first_name.required' => __('First Name is required.'),
            'last_name.required' => __('Last Name is required.'),
            'phone.max' => __('Telephone Number should be less than 20 characters long.'),
            'email.required' => __('Email is required.'),
            'email.email' => __('Email must be a valid email address.'),
            'email.unique' => __('This Email has already been taken.'),
            'password.required' => __('Password is required.'),
            'password_confirmation.required' => __('Password Confirmation is required.'),
            'password.min' => __('Password should be more than 8 characters long'),
            'gender.required' => __('Gender is required.'),
            'country.required' => __('Country is required.'),
            'city.required' => __('City is required.'),
            'state.required' => __('Province is required.'),
            'job_title.required' => __('Job Titles is required.'),
            'job_type.required' => __('Job Types is required.'),
            'language.required' => __('Language is required.'),
            'education.required' => __('Level of Education is required.'),
            'experience.required' => __('Years of Experience is required.'),
            'postal_code.required' => __('Postal Code is required.'),
            'is_immediate_available.required' => __('Ready To Work is required.'),
            'willing_to_relocate.required' => __('Willing To Relocate is required.'),
            'profile_visibility.required' => __('Profile Visibility is required.'),
            'g-recaptcha-response.required' => __('Recaptcha is required.'),
        ];

        $this->validate($request, $rules, $customMessages);

        $user = new User();
            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
            $user->email = $request->input('email');
            $user->password = bcrypt($request->input('password'));
            $user->phone = $request->input('phone');
            $user->gender_id = $request->input('gender');
            $user->degree_level_id = $request->input('education');
            $user->country_id = $request->input('country');
            $user->state_id = $request->input('state');
            $user->city_id = $request->input('city');
            $user->street_address = $request->input('street_address');
            $user->postal_code = $request->input('postal_code');
            $user->is_immediate_available = $request->input('is_immediate_available');
            $user->willing_to_relocate = $request->input('willing_to_relocate');
            $user->expected_salary = $request->input('expected_salary');
            $user->salary_period_id = $request->input('salary_period');
            $user->job_experience_id = $request->input('experience');
            $user->profile_visibility = $request->input('profile_visibility');
            $user->user_type = $request->input('candidate_or_employer');
            $user->is_active = 0;
            $user->verified = 0;
            $user->current_status = 'pending_for_user_confirmation';

        if(isset($request->other_languages) && $request->other_languages != ''){
            $user->other_languages = $request->other_languages;
        }

        if(isset($request->custom_job_title) && $request->custom_job_title != ''){
            $user->other_job_title = $request->custom_job_title;

            $otherJobTitles = explode( ',', $request->custom_job_title );
            foreach ($otherJobTitles as $otherJobTitle){
                $exitOtherJobTitle = JobTitleOther::where('job_title',trim($otherJobTitle))->first();
                if(!$exitOtherJobTitle){
                    $otherJobTitleObj= new JobTitleOther();
                    $otherJobTitleObj->job_title=trim($otherJobTitle);
                    $otherJobTitleObj->is_active=1;
                    $otherJobTitleObj->save();
                }
            }
        }

            $user->save();

            foreach ($request->job_title as $job_title){
                if($job_title != 'custom'){
                    $userJobTitle = new UserJobTitle();
                    $userJobTitle->user_id = $user->id;
                    $userJobTitle->job_title_id = $job_title;
                    $userJobTitle->save();
                }
            }

            foreach ($request->job_type as $job_type) {
                $userJobType = new UserJobType();
                $userJobType->user_id = $user->id;
                $userJobType->job_type_id = $job_type;
                $userJobType->save();
            }

            foreach ($request->language as $language) {
                $userLanguage= new UserLanguage();
                $userLanguage->user_id = $user->id;
                $userLanguage->language_id = $language;
                $userLanguage->save();
            }
            $user->name = $user->getName();
            $user->update();
            return redirect()->route('registration_preview',  ['id' => $user->id]);
    }

    public function registrationPreview($id)
    {
        $user = User::where('id', $id)->first();

        $terms = DB::table('cms')
            ->join('cms_content', 'cms_content.page_id', '=', 'cms.id')
            ->where('cms.page_slug', 'terms-of-use')
            ->first();


        $terms = $terms ? $terms->page_content : '';
        return view('auth.register_preview')->with(['user' => $user, 'terms'=>$terms]);
    }

    public function registrationUpdate(UserFrontRegisterEditFormRequest $request, $id)
    {
        $user = User::find($id);
        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->phone = $request->input('phone');
        $user->gender_id = $request->input('gender');
        $user->degree_level_id = $request->input('education');
        $user->country_id = $request->input('country');
        $user->state_id = $request->input('state');
        $user->city_id = $request->input('city');
        $user->street_address = $request->input('street_address');
        $user->postal_code = $request->input('postal_code');
        $user->is_immediate_available = $request->input('is_immediate_available');
        $user->willing_to_relocate = $request->input('willing_to_relocate');
        $user->expected_salary = $request->input('expected_salary');
        $user->salary_period_id = $request->input('salary_period');
        $user->job_experience_id = $request->input('experience');
        $user->profile_visibility = $request->input('profile_visibility');
        $user->user_type = $request->input('candidate_or_employer');
        $user->is_active = 0;
        $user->verified = 0;
        $user->current_status = 'pending_for_user_confirmation';
        $user->password = Hash::make($request->password);

        $user->other_languages = $request->other_languages;

        $user->other_job_title = $request->custom_job_title;

        if(isset($request->custom_job_title) && $request->custom_job_title != ''){
            $otherJobTitles = explode( ',', $request->custom_job_title );
            foreach ($otherJobTitles as $otherJobTitle){
                $exitOtherJobTitle = JobTitleOther::where('job_title',trim($otherJobTitle))->first();
                if(!$exitOtherJobTitle){
                    $otherJobTitleObj= new JobTitleOther();
                    $otherJobTitleObj->job_title=trim($otherJobTitle);
                    $otherJobTitleObj->is_active=1;
                    $otherJobTitleObj->save();
                }
            }
        }

        $user->userJobTitles()->delete();
        foreach ($request->job_title as $job_title){
            $userJobTitle = new UserJobTitle();
            $userJobTitle->user_id = $user->id;
            $userJobTitle->job_title_id = $job_title;
            $userJobTitle->save();
        }

        $user->userJobTypes()->delete();
        foreach ($request->job_type as $job_type) {
            $userJobType = new UserJobType();
            $userJobType->user_id = $user->id;
            $userJobType->job_type_id = $job_type;
            $userJobType->save();
        }

        $user->userLanguages()->delete();
        foreach ($request->language as $language) {
            $userLanguage= new UserLanguage();
            $userLanguage->user_id = $user->id;
            $userLanguage->language_id = $language;
            $userLanguage->save();
        }
        $user->name = $user->getName();
        $user->current_status = 'registration_confirmed';
        $user->update();

        return redirect()->route('registration_preview', ['id'=>$user->id]);
    }

    public function registrationConfirm($id)
    {
        $user = User::find($id);
        $user->is_active = 1;
        $user->is_accept_terms = 1;
        $user->update();
        $user->sendEmailVerificationNotification();
        return redirect()->route('confirmation_page', ['id'=>$id]);
    }

    public function confirmationPage($id)
    {
        $user = User::find($id);
        return view('auth.confirmation')
            ->with('user', $user);
    }

    public function registrationEdit($id)
    {
        $user = User::where('id', $id)->first();
        $genders = Gender::where('is_active',1)->orderBy('sort_order', 'ASC')->get();
        $countries = Country::where('country', 'Canada')->orderBy('id', 'ASC')->get();
        $countryId = Country::where('country', 'Canada')->first()->id;
        $states = State::where('is_active',1)->where('country_id', $countryId)->orderBy('state', 'ASC')->get();
        $jobTitles = JobTitle::where('is_active',1)->orderBy('job_title', 'ASC')->get();
        $jobTypes = JobType::where('is_active',1)->orderBy('sort_order', 'ASC')->get();
        $degreeLevels = DegreeLevel::where('is_active',1)->orderBy('sort_order', 'ASC')->get();
        $languages = Language::where('is_active',1)->orderBy('sort_order', 'ASC')->get();
        $salaryPeriods = SalaryPeriod::where('is_active',1)->orderBy('sort_order', 'ASC')->get();
        $jobExperiences = JobExperience::where('is_active',1)->orderBy('sort_order', 'ASC')->get();
        $userJobTitles = [];
        $items = DB::table('user_job_titles')
            ->join('job_titles','job_titles.id','=','user_job_titles.job_title_id')
            ->where('user_id', $user->id)->get();
        foreach ($items as $item){
            array_push($userJobTitles, $item->id);
        }

        $userJobTypes= [];
        $items = DB::table('user_job_types')
            ->join('job_types','job_types.id','=','user_job_types.job_type_id')
            ->where('user_id', $user->id)->get();
        foreach ($items as $item){
            array_push($userJobTypes, $item->id);
        }

        $userLanguages= [];
        $items = DB::table('user_languages')
            ->join('languages','languages.id','=','user_languages.language_id')
            ->where('user_id', $user->id)->get();
        foreach ($items as $item){
            array_push($userLanguages, $item->id);
        }
        return view('auth.register_edit')
            ->with('user', $user)
            ->with('genders', $genders)
            ->with('countries', $countries)
            ->with('states', $states)
            ->with('jobTitles', $jobTitles)
            ->with('jobTypes', $jobTypes)
            ->with('languages', $languages)
            ->with('salaryPeriods', $salaryPeriods)
            ->with('jobExperiences', $jobExperiences)
            ->with('userJobTitles', $userJobTitles)
            ->with('userJobTypes', $userJobTypes)
            ->with('userLanguages', $userLanguages)
            ->with('degreeLevels', $degreeLevels);
    }

    public function registrationCancel($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('home');
    }
}
