<?php

namespace App\Http\Controllers;

use DB;
use App\Faq;
use App\Seo;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $faqs = Faq::select(
                        [
                            'faqs.id',
                            'faqs.faq_question',
                            'faqs.faq_answer',
                            'faqs.sort_order',
                            'faqs.created_at',
                            'faqs.updated_at'
                        ]
                )
                ->lang()
                ->orderBy('faqs.sort_order', 'ASC')
                ->orderBy('faqs.id', 'ASC')
                ->get();
        //print_r($seo);exit;
        $seo = (object) array(
            'seo_title' => 'JOBSEEKERS  FAQ’S - Hospitality, Travel &amp; Tourism Recruitment Post Covid',
            'seo_description' => 'From mom and pop to large corporate type businesses, we aim to make it extremely efficient to hire staff for all HTT type jobs by putting Employers directly in touch with Jobseekers. So simple & so unique!',
            'seo_keywords' => 'Hospitality Travel Tourism Staffing Recruitment Jobseekers Employers PostCovid Covid simple unique Efficient resume video job',
            'seo_other' => '<meta name="robots" content="ALL, FOLLOW,INDEX" />'
        );
        return view('faq.list_faq')
            ->with('faqs', $faqs)
            ->with('seo', $seo);
    }

}
