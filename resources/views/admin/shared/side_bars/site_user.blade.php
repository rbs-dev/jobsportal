
<li class="nav-item {{ Request::is('admin/list-users')?'open':''}}{{ Request::is('admin/profile-summary/*')?'open':''}}{{ Request::is('admin/profile-videos')?'open':''}}{{ Request::is('admin/deleted-user')?'open':''}}"> <a href="javascript:;" class="nav-link nav-toggle"> <i class="icon-user"></i> <span class="title">Jobseeker</span> <span class="arrow {{ Request::is('admin/list-users')?'open':''}}{{ Request::is('admin/profile-summary/*')?'open':''}}{{ Request::is('admin/profile-videos')?'open':''}}{{ Request::is('admin/deleted-user')?'open':''}}"></span> </a>
    <ul class="sub-menu" style="{{ Request::is('admin/list-users')?'display: block':''}}{{ Request::is('admin/profile-summary/*')?'display: block':''}}{{ Request::is('admin/profile-videos')?'display: block':''}}{{ Request::is('admin/deleted-user')?'display: block':''}}">
        <li class="nav-item {{ Request::is('admin/list-users')?'active':''}}"> <a href="{{ route('list.users') }}" class="nav-link "> <i class="icon-user"></i> <span class="title">List Jobseeker Profiles</span> </a> </li>
       <!--  <li class="nav-item  "> <a href="{{ route('create.user') }}" class="nav-link "> <i class="icon-user"></i> <span class="title">Add new User Profile</span> </a> </li> -->

         <li class="nav-item {{ Request::is('admin/profile-videos')?'active':''}}">
             <a href="{{ route('list.admin.profile_video') }}" class="nav-link "> <i class="icon-camera"></i> <span class="title">Profile Videos</span> </a>
        </li>

        <li class="nav-item {{ Request::is('admin/deleted-user')?'active':''}}">
            <a href="{{ route('list.admin.deleted_user') }}" class="nav-link "> <i class="icon-user"></i> <span class="title">Deleted Jobseekers</span> </a>
        </li>
    </ul>
</li>