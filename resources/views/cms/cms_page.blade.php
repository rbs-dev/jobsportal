@extends('layouts.app')
@section('content')
<!-- Header start -->
@include('includes.header')
<!-- Header end --> 
<!-- Inner Page Title start -->
@include('includes.inner_page_title')

<section id="title">
    <div class="title-inner">
        <div class="container">
            <div class="imr">
                <div class="im-12">
                    <div class="title">
                        <h2>{{$cmsContent->page_title}}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<div class="page-contents">
    @if($cms->page_slug != 'how-it-works' and $cms->page_slug != 'contact-us')
        {!! $cmsContent->page_content !!}
    @endif
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">{!! $siteSetting->cms_page_ad !!}</div>
        <div class="col-md-3"></div>
    </div>

    @if($cms->page_slug == 'contact-us')

        <section id="jobseeker">
            <div class="container">
                @include('flash::message')
                <div class="imr">
                    <div class="im-12">
                        <div class="main-body mt-0 pt-0">
                            {!! $cmsContent->page_content !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="imr">
                    <div class="im-12">
                        <div class="jobseeker-inner">
                            <h2>Contact us</h2>
                            <div class="jobseeker-form">
                                <div class="form-items">
                                    <form name="contactUs_form" class="form-horizontal" method="POST" action="{{ route('contact_us_process') }}">
                                        {{ csrf_field() }}
                                        <div class="single-input full-width {{ $errors->has('first_name') ? ' has-error' : '' }}">
                                            <label for="email">First Name </label>
                                            <input id="first_name" type="text" name="first_name" value="{{ old('first_name') }}" required="required" autofocus placeholder="{{__('First Name')}}">
                                            @if ($errors->has('first_name'))
                                                <span class="help-block custom-help-block">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                            @endif
                                        </div>

                                        <div class="single-input full-width {{ $errors->has('last_name') ? ' has-error' : '' }}">
                                            <label for="email">Last Name </label>
                                            <input id="last_name" type="text" name="last_name" value="{{ old('last_name') }}" required="required" autofocus placeholder="{{__('Last Name')}}">
                                            @if ($errors->has('last_name'))
                                                <span class="help-block custom-help-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                            @endif
                                        </div>


                                        <div class="single-input full-width {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label for="email">Email </label>
                                            <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus placeholder="{{__('Email Address')}}">
                                            @if ($errors->has('email'))
                                                <span class="help-block custom-help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="single-input full-width {{ $errors->has('subject') ? ' has-error' : '' }}">
                                            <label for="subject">Subject </label>
                                            <input id="subject" type="text" name="subject" value="{{ old('subject') }}" required placeholder="{{__('Subject')}}">
                                            @if ($errors->has('subject'))
                                                <span class="help-block custom-help-block">
                                                    <strong>{{ $errors->first('subject') }}</strong>
                                                </span>
                                            @endif
                                        </div>

                                        <div class="single-input full-width {{ $errors->has('body') ? ' has-error' : '' }}">
                                            <label for="subject">Body </label>
                                            <textarea id="body" rows="10" name="body" required="required" placeholder="{{__('Body')}}">{{ old('body') }}</textarea>
                                            @if ($errors->has('body'))
                                                <span class="help-block custom-help-block">
                                            <strong>{{ $errors->first('body') }}</strong>
                                        </span>
                                            @endif
                                        </div>

                                        <div style="padding-top: 20px;" class="single-input">
                                            <div id="formrowRecaptcha" class="formrow {{ $errors->has('g-recaptcha-response') ? ' has-error' : '' }}">
                                                {!! app('captcha')->display() !!}
                                                @if ($errors->has('g-recaptcha-response'))
                                                    <span class="help-block  custom-help-block">
                                                <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="single-submit-button">
                                            <input type="submit" value="Send Email">
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
            @push('scripts')
                <script>

                    contactUsFormValidation();
                    $.validator.addMethod('reCaptchaMethod', function (value, element, param) {
                        if (grecaptcha.getResponse() == ''){
                            return false;
                        } else {
                            // I would like also to check server side if the recaptcha response is good
                            return true
                        }
                    });
                    function contactUsFormValidation() {
                        var form3 = $('form[name=contactUs_form]');
                        var error3 = $('.alert-danger', form3);
                        var success3 = $('.alert-success', form3);
                        form3.validate({
                            errorElement: 'span', //default input error message container
                            errorClass: 'custom-help-block', // default input error message class
                            focusInvalid: false, // do not focus the last invalid input
                            ignore: "", // validate all fields including form hidden input
                            rules: {
                                'first_name': {
                                    required: true
                                },
                                'last_name': {
                                    required: true
                                },
                                'email': {
                                    required: true,
                                    email: true
                                },
                                'subject': {
                                    required: true
                                },
                                'body': {
                                    required: true
                                },
                                'g-recaptcha-response': {
                                    reCaptchaMethod: true
                                }
                            },

                            messages: { // custom messages for radio buttons and checkboxes
                                first_name: {
                                    required: "First Name is required."
                                },
                                last_name: {
                                    required: "Last Name is required."
                                },
                                email: {
                                    required: "Email is required.",
                                    email: 'Email must be a valid email address.'
                                },

                                subject: {
                                    required: "Subject is required."
                                },
                                body: {
                                    required: "Body is required."
                                },
                                'g-recaptcha-response': {
                                    reCaptchaMethod: "Recaptcha is required."
                                },
                            },

                            errorPlacement: function (error, element) { // render error placement for each input type
                                if (element.parent(".input-group").length > 0) {
                                    error.insertAfter(element.parent(".input-group"));
                                } else if (element.attr("data-error-container")) {
                                    error.appendTo(element.attr("data-error-container"));
                                } else if (element.parents('.radio-list').length > 0) {
                                    error.appendTo(element.parents('.radio-list').attr("data-error-container"));
                                } else if (element.parents('.form-check-inline').length > 0) {
                                    error.appendTo(element.parents('.col-md-6').find('.errorMessage').show());
                                } else if (element.parents('.checkbox-list').length > 0) {
                                    error.appendTo(element.parents('.checkbox-list').attr("data-error-container"));
                                } else if (element.parents('.select2Dropdown').length > 0) {
                                    error.appendTo(element.parents('.select2Dropdown').find('.errorMessage').show());
                                } else {
                                    error.insertAfter(element); // for other inputs, just perform default behavior
                                }
                            },

                            invalidHandler: function (event, validator) { //display error alert on form submit
                                success3.hide();
                                error3.show();
                                if (!validator.numberOfInvalids())
                                    return;
                                if($(validator.errorList[0].element).attr('id')=='g-recaptcha-response'){
                                    $('html, body').animate({
                                        scrollTop: $('#formrowRecaptcha').offset().top
                                    }, 2000);
                                }else{
                                    $('html, body').animate({
                                        scrollTop: $(validator.errorList[0].element).offset().top
                                    }, 2000);
                                }
                            },

                            highlight: function (element) { // hightlight error inputs
                                $(element)
                                    .closest('.formrow').addClass('has-error'); // set error class to the control group
                                // $(document).scrollTo('.has-error', 2000);
                            },

                            unhighlight: function (element) { // revert the change done by hightlight
                                $(element)
                                    .closest('.formrow').removeClass('has-error'); // set error class to the control group
                            },

                            success: function (label) {
                                label
                                    .closest('.formrow').removeClass('has-error'); // set success class to the control group
                            },

                            submitHandler: function (form) {
                                success3.show();
                                error3.hide();
                                form[0].submit(); // submit the form
                            }
                        });
                    }



                </script>
            @endpush
    @endif

    @if($cms->page_slug == 'how-it-works')
        <div class="section white">
            <div class="container">
                <div class="imr">
                    <div class="im-12">
                        <div class="section-contents">
                            <video controls width="100%" height="500px" id="vid" src="{{ asset('/videos/videojp.mp4') }}"></video>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

</div>


@include('includes.footer_social')
@endsection