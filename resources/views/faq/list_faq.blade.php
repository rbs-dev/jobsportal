@extends('layouts.app')
@section('content')
<!-- Header start -->
@include('includes.header')
<!-- Header end --> 
<!-- Inner Page Title start -->
@include('includes.inner_page_title', ['page_title'=>__('Frequently asked questions')])
<!-- Inner Page Title end -->
<!-- Page Title End -->
<section id="title">
    <div class="title-inner">
        <div class="container">
            <div class="imr">
                <div class="im-12">
                    <div class="title">
                        <h2>JOBSEEKERS  FAQ’S</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="section white">
    <div class="container">
        <div class="imr">
            <div class="im-12">
                <div class="section-contents">

                    <div class=" accordion_one">
                        <div class="panel-group" id="accordion">

                            @if(isset($faqs) && count($faqs))
                                @foreach($faqs as $faq)
                                    <div class="panel panel-default">
                                        <div class="panel-heading"  id="heading{{ $faq->id }}">
                                            <h4 class="panel-title">
                                                <a aria-expanded="false" data-toggle="collapse" class="collapsed" href="#collapse{{ $faq->id }}">{!! $faq->faq_question !!}</a>
                                            </h4>
                                        </div>
                                        <div data-parent="#accordion" aria-labelledby="heading{{ $faq->id }}" id="collapse{{ $faq->id }}" class="panel-collapse collapse">
                                            <div class="panel-body">{!! $faq->faq_answer !!}</div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">{!! $siteSetting->cms_page_ad !!}</div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('includes.footer_social')
@endsection

@push('styles')

    {{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">--}}
    <link href="{{asset('/')}}css/custom-bootstrap.min.css" rel="stylesheet">

    <style type="text/css">
        .accordion_one .panel-group {
            border: 1px solid #f1f1f1;
            margin-top: 100px
        }

        a:link {
            text-decoration: none
        }

        .accordion_one .panel {
            background-color: transparent;
            box-shadow: none;
            border-bottom: 0px solid transparent;
            border-radius: 0;
            margin: 0
        }

        .accordion_one .panel-default {
            border: 0
        }

        .accordion-wrap .panel-heading {
            padding: 0px;
            border-radius: 0px
        }

        h4 {
            font-size: 18px;
            line-height: 24px
        }

        .accordion_one .panel .panel-heading a.collapsed {
            display: block;
            padding: 12px 30px;
            border-top: 0px
        }

        .accordion_one .panel .panel-heading a {
            display: block;
            padding: 12px 30px;
            background: #fff;
            border-bottom: 1px solid #f1f1f1
        }

        .accordion-wrap .panel .panel-heading a {
            font-size: 14px
        }

        .accordion_one .panel-group .panel-heading+.panel-collapse>.panel-body {
            border-top: 0;
            padding-top: 0;
            padding: 25px 30px 30px 35px;
            background: #fff;
            color: #000
        }

        .img-accordion {
            width: 81px;
            float: left;
            margin-right: 15px;
            display: block
        }

        .accordion_one .panel .panel-heading a.collapsed:after {
            content: "\2b";
            color: #999999;
            background: #f1f1f1
        }

        .accordion_one .panel .panel-heading a:after,
        .accordion_one .panel .panel-heading a.collapsed:after {
            font-family: 'FontAwesome';
            font-size: 15px;
            width: 36px;
            line-height: 48px;
            text-align: center;
            background: #F1F1F1;
            float: left;
            margin-left: -31px;
            margin-top: -12px;
            margin-right: 15px
        }

        .accordion_one .panel .panel-heading a:after {
            content: "\2212"
        }

        .accordion_one .panel .panel-heading a:after,
        .accordion_one .panel .panel-heading a.collapsed:after {
            font-family: 'FontAwesome';
            font-size: 15px;
            width: 36px;
            height: 48px;
            line-height: 48px;
            text-align: center;
            background: #F1F1F1;
            float: left;
            margin-left: -31px;
            margin-top: -12px;
            margin-right: 15px
        }
    </style>
@endpush
