@extends('layouts.app')
@section('content')
<!-- Header start -->
@include('includes.header')
<!-- Header end -->
<!-- Inner Page Title start -->
@include('includes.inner_page_title', ['page_title'=>__('Login')])
<!-- Inner Page Title end -->

<section id="title">
    <div class="title-inner">
        <div class="container">
            <div class="imr">
                <div class="im-12">
                    <div class="title">
                        <h2>Login</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="jobseeker">
    <div class="container">
            <div class="mb-5">
                @include('flash::message')
            </div>
        <div class="imr">
            <div class="im-12">
                <div class="jobseeker-inner">
                    {{--<h2>Jobseeker</h2>--}}
                    <div class="jobseeker-form">
                        <div class="form-items">
                            <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <input type="hidden" name="candidate_or_employer" value="candidate"/>

                                <div class="single-input full-width {{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email">Email </label>
                                    <input id="email" type="email" name="email" value="{{ old('email') }}" required autofocus placeholder="{{__('Email Address')}}">
                                    @if ($errors->has('email'))
                                        <span class="help-block custom-help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="single-input full-width {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="email">Password </label>
                                    <input id="password" type="password" class="form-control" name="password" value="" required placeholder="{{__('Password')}}">
                                    @if ($errors->has('password'))
                                        <span class="help-block custom-help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>

                                <div class="single-submit-button">
                                    <input type="submit" value="Login">
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
            <div class="im-12 mt-5">
                <div class="text-center"><i class="fa fa-user" aria-hidden="true"></i> {{__('New User')}}? <a href="{{route('register')}}">{{__('Register Here')}}</a></div>
                <div class="text-center"><i class="fa fa-user" aria-hidden="true"></i> {{__('Forgot Your Password')}}? <a href="{{ route('password.request') }}">{{__('Click here')}}</a></div>
            </div>
        </div>
    </div>
</section>

@include('includes.footer_social')
@endsection
