<?php
if (!isset($seo)) {
    $seo = (object)array('seo_title' => $siteSetting->site_name, 'seo_description' => $siteSetting->site_name, 'seo_keywords' => $siteSetting->site_name, 'seo_other' => '');
}
?>
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" class="{{ (session('localeDir', 'ltr'))}}" dir="{{ (session('localeDir', 'ltr'))}}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{__($seo->seo_title) }}</title>
    <meta name="Description" content="{!! $seo->seo_description !!}">
    <meta name="Keywords" content="{!! $seo->seo_keywords !!}">
    {!! $seo->seo_other !!}

    <link rel="shortcut icon" href="{{asset('/')}}images/favicon.png">
    {{--<link rel="stylesheet" href="{{asset('/')}}assets/css/font-awesome.min.css">--}}
    <link rel="stylesheet" href="{{asset('/')}}assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="{{asset('/')}}assets/css/all.css">
    <link rel="stylesheet" href="{{asset('/')}}assets/css/custom.css">
    <link rel="stylesheet" href="{{asset('/')}}assets/css/style.css">
    <link href="{{asset('/')}}css/star-rating-svg.css" rel="stylesheet">
    <link href="{{asset('/')}}css/custom/css/style.css" rel="stylesheet">
    <link href="{{asset('/')}}css/font-awesome.css" rel="stylesheet">


    @if((session('localeDir', 'ltr') == 'rtl'))
    <!-- Rtl Style -->
    <link href="{{asset('/')}}css/rtl-style.css" rel="stylesheet">
    @endif
    <link href="{{ asset('/') }}admin_assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/') }}admin_assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/') }}admin_assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="{{asset('/')}}js/html5shiv.min.js"></script>
          <script src="{{asset('/')}}js/respond.min.js"></script>
        <![endif]-->
    @stack('styles')
<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-LJS5NZQS2Y"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-LJS5NZQS2Y');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-JVPE4QTTWW"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-JVPE4QTTWW');
    </script>

    <!-- Global site tag (gtag.js) - Google Ads: 371238394 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-371238394"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'AW-371238394');
    </script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>

<body>
    @yield('content')

    {{--<script type="text/javascript" src="{{ asset('/') }}assets/js/jquery.slim.js"></script>--}}
    <script type="text/javascript" src="{{ asset('/') }}js/jquery.min.js"></script>
    {{--<script type="text/javascript" src="{{ asset('/') }}assets/js/popper.js"></script>--}}
    <script type="text/javascript" src="{{ asset('/') }}js/bootstrap.min.js"></script>
    <script type="text/javascript" src="{{ asset('/') }}assets/js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="{{ asset('/') }}assets/js/all.js"></script>
    <script type="text/javascript" src="{{ asset('/') }}assets/js/scripts.js"></script>
    {{--<script type="text/javascript" src="{{ asset('/') }}js/scripts.js"></script>--}}

    <script src="{{ asset('/') }}admin_assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
    <script src="{{ asset('/') }}admin_assets/global/plugins/Bootstrap-3-Typeahead/bootstrap3-typeahead.min.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <script src="{{ asset('/') }}admin_assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
    <script src="{{ asset('/') }}admin_assets/global/plugins/jquery.scrollTo.min.js" type="text/javascript"></script>
    <!-- Revolution Slider -->
    <script type="text/javascript" src="{{ asset('/') }}js/revolution-slider/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="{{ asset('/') }}js/revolution-slider/js/jquery.themepunch.revolution.min.js"></script>

    <script type="text/javascript" src="{{ asset('/') }}js/sweetalert.min.js"></script>
    <script type="text/javascript" src="{{ asset('/') }}js/jquery.validate.js"></script>
    <script type="text/javascript" src="{{ asset('/') }}js/jquery.star-rating-svg.js"></script>



    {!! NoCaptcha::renderJs() !!}
    @stack('scripts')
    <!-- Custom js -->
    <script src="{{asset('/')}}js/script.js"></script>
    <script type="text/JavaScript">
        $(document).ready(function(){
            // $(document).scrollTo('.has-error', 2000);
            });
            function showProcessingForm(btn_id){
            $("#"+btn_id).val( 'Processing .....' );
            $("#"+btn_id).attr('disabled','disabled');
            }

        $(document).ready(function() {
            var isshown = sessionStorage.getItem('isshown');
            if (isshown== null) {
                sessionStorage.setItem('isshown',1);
                setTimeout(function(){
                    $('#myModal').modal('show');
                }, 3500);

            }
        });

        </script>
    @if(!Auth::user() && !Auth::guard('company')->user() && !Request::is('login') && !Request::is('password/reset') && !Request::is('password/reset/*'))
    <div id="myModal" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button style="background:#EA764F; font-weight: bold; color: #fff;" type="button" class="close" data-dismiss="modal">X</button>
                </div>
                <div style="padding: 20px 30px;" class="modal-body termsModal about-wraper">
                       <h4 class="text-center mb-4" style="background:#FCC449; padding: 20px 0; font-weight: bold;">
                           Attention: Hospitality, Travel &amp; Tourism Jobseekers and<br>

                            Employers!<br>

                           Time to find Work &amp; Re-Open!!!
                       </h4>

                    <div class="pop-up-body-text">
                        <p>
                            Click on the &quot;How it Works&quot; Video to see the &quot;POST COVID&quot;
                            opportunity for all Hospitality, Travel &amp; Tourism Employers and
                            Jobseekers!
                        </p>
                        <p>
                            The site is currently open to Jobseekers ONLY to register their
                            FREE profiles. It only takes minutes!
                        </p>
                        <p>
                            Employers are anxious to start hiring and in order to open it up to
                            them, we need all HTT Jobseekers to register ASAP.
                        </p>
                        <p>
                            Once there is a minimum or 1,000 Jobseekers registered in the
                            &quot;first &quot; province , we will open it up to Employers to &quot;find you&quot;!
                        </p>
                        <p>
                            Check the &quot;province count&quot; at the bottom to keep updated.
                        </p>
                    </div>


                        <h4  style="background:#FCC449; padding: 10px 20px;" class="text-right mt-4">HTTConnect Team</h4>

                </div>
            </div>
        </div>
    </div>
        @elseif(Auth::user()&&Auth::user()->email_verified_at==null)
        <div id="myModal" class="modal fade">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">X</button>
                    </div>
                    <div style="padding: 20px 30px" class="modal-body termsModal about-wraper">
                        <h4 class="text-center mb-4">
                            Attention: Hospitality, Travel &amp; Tourism Jobseekers <br>

                            and Employers!<br>

                            Time to find Work &amp; Re-Open!!!
                        </h4>

                        <p>
                            Click on the &quot;How it Works&quot; Video to see the &quot;POST COVID&quot;
                            opportunity for all Hospitality, Travel &amp; Tourism Employers and
                            Jobseekers!
                        </p>
                        <p>
                            The site is currently open to Jobseekers ONLY to register their
                            FREE profiles. It only takes minutes!
                        </p>
                        <p>
                            Employers are anxious to start hiring and in order to open it up to
                            them, we need all HTT Jobseekers to register ASAP.
                        </p>
                        <p>
                            Once there is a minimum or 1,000 Jobseekers registered in the
                            &quot;first &quot; province , we will open it up to Employers to &quot;find you&quot;!
                        </p>
                        <p>
                            Check the &quot;province count&quot; at the bottom to keep updated.
                        </p>

                        <h4 class="text-right mt-5">HTTConnect Team</h4>

                    </div>
                </div>
            </div>
        </div>
        @endif
</body>

</html>
