@extends('layouts.app')
@section('content')
<!-- Header start -->
@include('includes.header')
<!-- Header end -->
@include('includes.employer_tab')


<section id="dashboard">
    <div class="container">
        <div class="imr">
            <div class="im-12">
                <div class="dashboard-inner">
                    @include('flash::message')
                    @include('includes.employer_dashboard_menu')

                    <div class="dashboard-tab-content">

                        @include('employer.custom-employer-profile')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('includes.footer_social')
@endsection
@push('styles')
    <link href="{{asset('/')}}css/custom-bootstrap.min.css" rel="stylesheet">
<style type="text/css">
    .userccount p{ text-align:left !important;}
</style>
@endpush
@push('scripts')
    <script>
    $('.profile_image_remove').on('click', function () {
        var element = $(this);
        var url = $(this).attr('data-action');
        var blankImageUrl = $(this).attr('data-image-url');
        if(confirm('Are You Sure Delete Profile Image?')){
            $.get(url, function( data ) {
                if(data.status==200){
                    element.remove();
                    $('.addedImage').attr('src',blankImageUrl);
                    // $('.addedImage').remove();
                }
            });
        }

    })
    </script>

@include('includes.immediate_available_btn')
@endpush